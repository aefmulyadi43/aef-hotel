<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="image/favicon.png" type="image/png">
    <title>Aef Hotel</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('royal/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{ asset('royal/vendors/linericon/style.css')}}">
    <link rel="stylesheet" href="{{ asset('royal/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('royal/vendors/owl-carousel/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('royal/vendors/bootstrap-datepicker/bootstrap-datetimepicker.min.css')}}">
    <link rel="stylesheet" href="{{ asset('royal/vendors/nice-select/css/nice-select.css')}}">
    <link rel="stylesheet" href="{{ asset('royal/vendors/owl-carousel/owl.carousel.min.css')}}">
    <!-- main css -->
    <link rel="stylesheet" href="{{ asset('royal/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('royal/css/responsive.css')}}">
</head>
<body>
    @include('layouts.header-tamu')



    @yield('content')
    <!--================ Recent Area  =================-->
    @include('layouts.footer-tamu')


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset('royal/js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{ asset('royal/js/popper.js')}}"></script>
    <script src="{{ asset('royal/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('royal/vendors/owl-carousel/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('royal/js/jquery.ajaxchimp.min.js')}}"></script>
    <script src="{{ asset('royal/js/mail-script.js')}}"></script>
    <script src="{{ asset('royal/vendors/bootstrap-datepicker/bootstrap-datetimepicker.min.js')}}"></script>
    <script src="{{ asset('royal/vendors/nice-select/js/jquery.nice-select.js')}}"></script>
    <script src="{{ asset('royal/js/mail-script.js')}}"></script>
    <script src="{{ asset('royal/js/stellar.js')}}"></script>
    <script src="{{ asset('royal/vendors/lightbox/simpleLightbox.min.js')}}"></script>
    <script src="{{ asset('royal/js/custom.js')}}"></script>
</body>
</html>
