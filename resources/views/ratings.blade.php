@extends('layouts.app-tamu')
@section('content')
<!--================ Accomodation Area  =================-->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<style type="text/css">
    .feedback {
        width: 100%;
        max-width: 780px;
        background: #fff;
        margin: 0 auto;
        padding: 15px;
        box-shadow: 1px 1px 16px rgba(0, 0, 0, 0.3);
    }

    .survey-hr {
        margin: 10px 0;
        border: .5px solid #ddd;
    }

    .feedback form input,
    .feedback form textarea {
        width: 100%;
        border: 1px solid #ddd;
    }

    .star-rating {
        margin: 25px 0 0px;
        font-size: 0;
        white-space: nowrap;
        display: inline-block;
        width: 175px;
        height: 35px;
        overflow: hidden;
        position: relative;
        background: url('data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iMjBweCIgaGVpZ2h0PSIyMHB4IiB2aWV3Qm94PSIwIDAgMjAgMjAiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDIwIDIwIiB4bWw6c3BhY2U9InByZXNlcnZlIj48cG9seWdvbiBmaWxsPSIjREREREREIiBwb2ludHM9IjEwLDAgMTMuMDksNi41ODMgMjAsNy42MzkgMTUsMTIuNzY0IDE2LjE4LDIwIDEwLDE2LjU4MyAzLjgyLDIwIDUsMTIuNzY0IDAsNy42MzkgNi45MSw2LjU4MyAiLz48L3N2Zz4=');
        background-size: contain;
    }

    .star-rating i {
        opacity: 0;
        position: absolute;
        left: 0;
        top: 0;
        height: 100%;
        width: 20%;
        z-index: 1;
        background: url('data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iMjBweCIgaGVpZ2h0PSIyMHB4IiB2aWV3Qm94PSIwIDAgMjAgMjAiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDIwIDIwIiB4bWw6c3BhY2U9InByZXNlcnZlIj48cG9seWdvbiBmaWxsPSIjRkZERjg4IiBwb2ludHM9IjEwLDAgMTMuMDksNi41ODMgMjAsNy42MzkgMTUsMTIuNzY0IDE2LjE4LDIwIDEwLDE2LjU4MyAzLjgyLDIwIDUsMTIuNzY0IDAsNy42MzkgNi45MSw2LjU4MyAiLz48L3N2Zz4=');
        background-size: contain;
    }

    .star-rating input {
        -moz-appearance: none;
        -webkit-appearance: none;
        opacity: 0;
        display: inline-block;
        width: 20%;
        height: 100%;
        margin: 0;
        padding: 0;
        z-index: 2;
        position: relative;
    }

    .star-rating input:hover+i,
    .star-rating input:checked+i {
        opacity: 1;
    }

    .star-rating i~i {
        width: 40%;
    }

    .star-rating i~i~i {
        width: 60%;
    }

    .star-rating i~i~i~i {
        width: 80%;
    }

    .star-rating i~i~i~i~i {
        width: 100%;
    }

    .choice {
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        text-align: center;
        padding: 20px;
        display: block;
    }

    span.scale-rating {
        margin: 5px 0 15px;
        display: inline-block;

        width: 100%;

    }

    span.scale-rating>label {
        position: relative;
        -webkit-appearance: none;
        outline: 0 !important;
        border: 1px solid grey;
        height: 33px;
        margin: 0 5px 0 0;
        width: calc(10% - 7px);
        float: left;
        cursor: pointer;
    }

    span.scale-rating label {
        position: relative;
        -webkit-appearance: none;
        outline: 0 !important;
        height: 33px;

        margin: 0 5px 0 0;
        width: calc(10% - 7px);
        float: left;
        cursor: pointer;
    }

    span.scale-rating input[type=radio] {
        position: absolute;
        -webkit-appearance: none;
        opacity: 0;
        outline: 0 !important;
        /*border-right: 1px solid grey;*/
        height: 33px;

        margin: 0 5px 0 0;

        width: 100%;
        float: left;
        cursor: pointer;
        z-index: 3;
    }

    span.scale-rating label:hover {
        background: #fddf8d;
    }

    span.scale-rating input[type=radio]:last-child {
        border-right: 0;
    }

    span.scale-rating label input[type=radio]:checked~label {
        -webkit-appearance: none;
        margin: 0;
        background: #fddf8d;
    }

    span.scale-rating label:before {
        content: attr(value);
        top: 7px;
        width: 100%;
        position: absolute;
        left: 0;
        right: 0;
        text-align: center;
        vertical-align: middle;
        z-index: 2;
    }

</style>

<!--================ Facilities Area  =================-->
<section class="facilities_area section_gap">
    <div class="feedback">
        <p>Dear Customer,<br>
            Terimakasih telah menginap di Aef-Hotel,</p>
        <h4>Beri kami penilaian terkait fasilitas dan pelayanan kami</h4>
        <form class="form-horizontal" action="{{ route('rating.store') }}" method="POST">
            @csrf
            <hr class="survey-hr">
            <label>1. Berlapa Bintang yg ingin anda berikat?</label><br><br />
            <div style="color:grey">
                <span style="float:left">
                    POOR
                </span>
                <span style="float:right">
                    BEST
                </span>
            <input type="hidden" value="{{$data['tamu_id']}}" name="tamu_id">
             <input type="hidden" value="{{$data['kamar_id']}}" name="kamar_id">
             <input type="hidden" value="{{$data['id']}}" name="id">
            </div>
            <span class="scale-rating">
                <label value="1">
                    <input type="radio" value="1" name="star">
                    <label style="width:100%;"></label>
                </label>
                <label value="2">
                    <input type="radio" value="2" name="star">
                    <label style="width:100%;"></label>
                </label>
                <label value="3">
                    <input type="radio" value="3" name="star">
                    <label style="width:100%;"></label>
                </label>
                <label value="4">
                    <input type="radio" value="4" name="star">
                    <label style="width:100%;"></label>
                </label>
                <label value="5">
                    <input type="radio" value="5" name="star">
                    <label style="width:100%;"></label>
                </label>

            </span>
            <div class="clear"></div>
            <hr class="survey-hr">
            <label for="m_3189847521540640526commentText">2. Apa komentar anda mengenai ini:</label><br /><br />
            <textarea cols="75" name="review" rows="5" style="100%"></textarea><br>
            <br>
            <div class="clear"></div>
            <input style="background:#43a7d5;color:#fff;padding:12px;border:0" type="submit" value="Submit your review">
        </form>
    </div>

</section>
<!--================ Accomodation Area  =================-->
@endsection
