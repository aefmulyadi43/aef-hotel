@extends('layouts.app-tamu')
@section('content')
<!--================ Accomodation Area  =================-->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<style type="text/css">
    table tr td,
    table tr th {
        font-size: 9pt;
    }

</style>

<!--================ Facilities Area  =================-->
<section class="facilities_area section_gap">
    <section class="button-area">
        <div class="container border-top-generic">
            <h3 class="text-heading title_color">Sample Buttons</h3>
            <div class="row">
                <div class="col-12 table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tipe Kamar</th>
                                <th>Nama Kamar</th>
                                <th>Harga</th>
                                <th>Qty Kamar</th>
                                <th>Jumlah Hari</th>
                                <th>Subtotal</th>
                                <th>status</th>
                                <th></th>

                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $no=0;
                            @endphp


                            @foreach($data['reservasi'] as $row)
                            @php
                            $no++;
                            @endphp
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$row['kamar']['tipeKamar']['tipe_kamar']}}</td>
                                <td>{{$row['kamar']['nama_kamar']}}</td>
                                <td>{{"Rp. ".number_format($row['kamar']['harga'])}}</td>
                                <td>{{$row['qty_kamar']}}</td>
                                <td>{{$row['lama']}}</td>
                                <td>{{"Rp. ".number_format($row['kamar']['harga'] * $row['qty_kamar'] * $row['lama'])}}</td>
                                <td>{{$row['status']}}</td>
                                <td>
                                    @if($row['status']=='CHECKIN' || $row['status']=='CHECKOUT')
                                    @php
                                    $review = \App\Models\Review::where(['tamu_id' => $row['tamu_id'], 'kamar_id' => $row['kamar_id']])->first();
                                    @endphp
                                    @if(!$review)
                                    <a class="btn btn-success float-left" href="{{ url('tamu/rating',$row['id']) }}">
                                        Review
                                    </a>
                                    @endif


                                    @endif
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <a class="btn btn-primary float-right" href="{{ url('reservasi/download',$row['id']) }}">
                                        Download
                                    </a>
                                </td>



                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>
        </div>
    </section>

</section>
<!--================ Accomodation Area  =================-->
@endsection
