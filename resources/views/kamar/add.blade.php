@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">@yield('content-title')</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Starter Page</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card card-info">
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="{{ route('kamar.store') }}" method="POST">
                        @csrf
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 col-form-label">Nama Kamar</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="nama_kamar" id="inputEmail3" placeholder="Contoh : * VVIP">
                                    @if ($errors->has('nama_kamar'))
                                    <span class="text-danger">{{ $errors->first('nama_kamar') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-3 col-form-label">Tipe Kamar</label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="tipe_id">
                                        <option value="">- Pilih Tipe Kamar-</option>
                                        @foreach($data as $row)
                                        <option value="{{$row['id']}}">{{$row['tipe_kamar']}}</option>
                                        @endforeach

                                    </select>
                                    @if ($errors->has('tipe_id'))
                                    <span class="text-danger">{{ $errors->first('tipe_id') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 col-form-label">Harga</label>
                                <div class="col-sm-9">
                                    <input type="number" class="form-control" name="harga" id="inputEmail3" placeholder="Contoh : * 500000">
                                    @if ($errors->has('harga'))
                                    <span class="text-danger">{{ $errors->first('harga') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 col-form-label">Jumlah</label>
                                <div class="col-sm-9">
                                    <input type="number" class="form-control" name="jumlah" id="inputEmail3" placeholder="Contoh : * 1">
                                    @if ($errors->has('number'))
                                    <span class="text-danger">{{ $errors->first('number') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 col-form-label">Deskripsi</label>
                                <div class="col-sm-9">
                                    <textarea id="summernote" name="deskripsi">
               
                                    </textarea>
                                    @if ($errors->has('deskripsi'))
                                    <span class="text-danger">{{ $errors->first('deskripsi') }}</span>
                                    @endif
                                </div>
                            </div>

                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-info">Simpan</button>
                            <a href="{{url('kamar')}}" class="btn btn-warning">Kembali</a>

                            <button type="reset" class="btn btn-danger ">Batal</button>
                        </div>
                        <!-- /.card-footer -->
                    </form>

                </div>
            </div>

        </div>
    </div>
</div>

</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
    </div>
</aside>

@endsection