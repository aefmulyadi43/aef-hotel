@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Kamar</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Invoice</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">



                    <!-- Main content -->
                    <div class="invoice p-3 mb-3">
                        <!-- title row -->
                        <div class="row">
                            <div class="col-12">
                                <h4>
                                    <i class="fas fa-archway"></i> Nama Kamar

                                </h4>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- info row -->
                        <div class="row invoice-info">
                            <div class="col-sm-3 invoice-col">

                                <address>
                                    <strong>Nama Kamar</strong><br>
                                    Tipe<br>
                                    Harga<br>
                                    Jumlah<br>
                                    Reservasi<br>
                                    Available
                                </address>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-9 invoice-col">

                                <address>
                                    <strong>{{$data['kamar']['nama_kamar']}}</strong><br>
                                    {{$data['kamar']['tipeKamar']['tipe_kamar']}}<br>
                                    {{"RP. ".number_format($data['kamar']['harga'])}}<br>
                                    {{$data['total']}} Kamar<br>
                                    {{$data['reservasi']}} Kamar<br>
                                    {{$data['available']}} Kamar
                                </address>
                            </div>
                            <!-- /.col -->

                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        <!-- Table row -->
                        <div class="row justify-content-center">
                            <div class="col-md-6">
                                <div class="card card-info">
                                    <div class="card-header">
                                        <h3 class="card-title">Galery</h3>
                                        <a href="{{ url('kamar/galery/add',$data['id']) }}" class="btn  btn-primary btn-sm float-right">Tambah</a>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <table id="example2" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th width="5%">No</th>
                                                    <th>gambar</th>
                                                    <th width="10%"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                $no=0;
                                                @endphp
                                                @foreach($data['galery'] as $rows)
                                                @php
                                                $no++;
                                                @endphp
                                                <tr>
                                                    <td>{{$no}}</td>
                                                    <td>
                                                        <img style="width: 150px; height:150px;" src="{{asset('images/'.$rows['foto'])}}">
                                                    </td>

                                                    <td>

                                                        <a href="{{ url('kamar/galery/delete',$rows['id']) }}">
                                                            <i class="nav-icon fas fa-trash-alt"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                @endforeach

                                            </tbody>

                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>

                            <div class="col-md-6">
                                <div class="card card-info">
                                    <div class="card-header">
                                        <h3 class="card-title">Fasilitas</h3>
                                        <a href="{{ url('kamar/fasilitas/add',$data['id']) }}" class="btn  btn-primary btn-sm float-right">Tambah</a>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <table id="example1" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Icon</th>
                                                    <th>Fasilitas</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                $no=0;
                                                @endphp
                                                @foreach($data['fasilitasKamar'] as $row)
                                                @php
                                                $no++;
                                                @endphp
                                                <tr>
                                                    <td>{{$no}}</td>
                                                    <td>
                                                        <img style="width: 50px; height:50px;" src="{{asset('images/'.$row['fasilitas']['icon'])}}">
                                                    </td>
                                                    <td>{{$row['fasilitas']['nama_fasilitas']}}</td>
                                                    <td>
                                                        <a href="{{ url('kamar/fasilitas/delete/fasilitas',$row['id']) }}">
                                                            <i class="nav-icon fas fa-trash-alt"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                @endforeach

                                            </tbody>

                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>

                        </div>

                        <!-- /.row -->


                    </div>
                    <!-- /.invoice -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
    </div>
</aside>

@endsection
