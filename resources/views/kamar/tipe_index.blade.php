@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">@yield('content-title')</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Starter Page</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                @if(Session::has('success'))
                <input type="hidden" value="success" id="success">
                @endif
                <div class="card card-info">
                    
                    <!-- /.card-header -->
                    <!-- form start -->
                    @if($edit == '0')
                    <div class="card-header">
                        <h3 class="card-title">Tambah Tipe Kamar</h3>
                    </div>
                    <form class="form-horizontal" action="{{ route('kamar.tipe.store') }}" method="POST">
                        @csrf
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 col-form-label">Tipe Kamar</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="tipe" id="inputEmail3" placeholder="Contoh : * VVIP">
                                    @if ($errors->has('tipe'))
                                    <span class="text-danger">{{ $errors->first('tipe') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-3 col-form-label">Status</label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="status">
                                        <option value="1">Aktif</option>
                                        <option value="1">Tidak Aktif</option>
                                    </select>
                                    @if ($errors->has('status'))
                                    <span class="text-danger">{{ $errors->first('tipe') }}</span>
                                    @endif
                                </div>
                            </div>

                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-info">Simpan</button>
                            <button type="reset" class="btn btn-warning float-right">Cancel</button>
                        </div>
                        <!-- /.card-footer -->
                    </form>
                    @else
                    <div class="card-header">
                        <h3 class="card-title">Edit Tipe Kamar</h3>
                    </div>
                    <form class="form-horizontal" action="{{ route('kamar.tipe.update') }}" method="POST">
                        @csrf
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 col-form-label">Tipe Kamar</label>
                                <div class="col-sm-9">
                                    <input type="hidden" name="id" value="{{$old->id}}">
                                    <input type="text" class="form-control" name="tipe" id="inputEmail3" value="{{$old->tipe_kamar}}">
                                    @if ($errors->has('tipe'))
                                    <span class="text-danger">{{ $errors->first('tipe') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-3 col-form-label">Status</label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="status">
                                        <option value="1" <?php if($old->status=='1'){ echo 'selected';} ?>>Aktif</option>
                                        <option value="0" <?php if($old->status=='0'){ echo 'selected';} ?>>Tidak Aktif</option>
                                    </select>
                                    @if ($errors->has('status'))
                                    <span class="text-danger">{{ $errors->first('tipe') }}</span>
                                    @endif
                                </div>
                            </div>

                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-info">Update</button>
                            <button type="reset" class="btn btn-warning float-right">Cancel</button>
                        </div>
                        <!-- /.card-footer -->
                    </form>
                    @endif
                </div>
            </div>


            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">List Tipe Kamar</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tipe Kamar</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $no = 0;
                                @endphp
                                @foreach($data as $row)
                                @php
                                $no++;
                                @endphp
                                <tr>
                                    <td>{{$no}}</td>
                                    <td>{{$row['tipe_kamar']}}</td>

                                    <td>
                                        @if($row->status=='1')
                                        <span class="text-success">Aktif</span>
                                        @else
                                        <span class="text-danger">Non Aktif</span>
                                        @endif
                                    </td>
                                    <td>
                                    <a href="{{ url('kamar/tipe',$row['id']) }}">
                                    <i class="nav-icon fas fa-pencil-alt"></i>
                                    </a>
                                        &nbsp;&nbsp;
                                        <i class="nav-icon fas fa-trash-alt"></i>
                                    </td>

                                </tr>
                                @endforeach

                            </tbody>

                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>

        </div>
    </div>
</div>

</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
    </div>
</aside>

@endsection