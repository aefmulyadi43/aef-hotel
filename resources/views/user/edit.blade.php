@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">@yield('content-title')</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Starter Page</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if(Session::has('success'))
                <input type="hidden" value="success" id="success">
                @endif
                <div class="card card-info">
                    
                    <!-- /.card-header -->
                    <!-- form start -->
                  
                    <div class="card-header">
                        <h3 class="card-title">Edit Role User</h3>
                    </div>
                    <form class="form-horizontal" action="{{ route('user.update') }}" method="POST">
                        @csrf
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 col-form-label">Email</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="email" disabled id="inputEmail3" value="{{$data['email']}}">
                                    <input type="hidden" name="id" value="{{$data['id']}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 col-form-label">Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="name" disabled id="inputEmail3" value="{{$data['name']}}">
                 
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-3 col-form-label">Role</label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="role">
                                        <option value="Visit" <?php if($data['role']=='Visit' || $data['role']==null){ echo "selected";} ?>>Visitor</option>
                                        <option value="Admin" <?php if($data['role']=='Admin'){echo "selected";} ?>>Admin</option>
                                        <option value="Reception" <?php if($data['role']=='Reception'){echo "selected";} ?>>Reception</option>
                                    </select>
                                    
                                </div>
                            </div>

                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-info">Simpan</button>
                            <button type="reset" class="btn btn-warning float-right">Cancel</button>
                        </div>
                        <!-- /.card-footer -->
                    </form>
                    
                </div>
            </div>


            

        </div>
    </div>
</div>

</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
    </div>
</aside>

@endsection