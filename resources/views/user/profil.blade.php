@extends('layouts.app')
@section('content')
<!--================ Accomodation Area  =================-->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <!-- Main content -->
                    <div class="invoice p-3 mb-3">
                        <!-- title row -->
                        <div class="row">
                            <div class="col-12">
                                <h4>
                                    Aef | Hotel

                                </h4>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- info row -->
                        <div class="row invoice-info">
                            <div class="col-sm-4 invoice-col">
                                <h5><strong>Profil</strong></h5>
                                <address>
                                    <strong>Nama</strong><br>
                                    Email<br>
                                    NIK<br>
                                    Jenis Kelamin<br>
                                    Alamat<br>
                                    Telephone<br>
                                    Access<br>

                                </address>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-8 invoice-col">
                                -
                                <address>
                                    <strong>{{$data['tamu']['nama']}}</strong><br>
                                    {{$data['user']['email']}}<br>
                                    {{$data['tamu']['nik']}}<br>
                                    {{$data['tamu']['jenis_kelamin']}}<br>
                                    {{$data['tamu']['Alamat']}}<br>
                                    {{$data['tamu']['telepon']}}<br>
                                    {{$data['user']['role']}}<br>

                                </address>
                            </div>
                            <!-- /.col -->
                            <div class="row no-print">
                                <div class="col-12">
{{--
                                    <a class="btn btn-success" href="{{route('profil.password')}}">
                                        Ubah Password
                                    </a> --}}
                                    <a class="btn btn-success" href="{{url('/password/reset')}}">
                                        Ubah Password
                                    </a>
                                    <a class="btn btn-primary" href="{{route('profil.edit')}}">
                                        Ubah Data Diri
                                    </a>

                                </div>
                            </div>
                        </div>


                    </div>
                    <!-- /.invoice -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!--================ Accomodation Area  =================-->
@endsection
