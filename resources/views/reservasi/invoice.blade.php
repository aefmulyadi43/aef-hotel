@extends('layouts.app')
@section('content')
<!--================ Accomodation Area  =================-->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <!-- Main content -->
                    <div class="invoice p-3 mb-3">
                        <!-- title row -->
                        <div class="row">
                            <div class="col-12">
                                <h4>
                                    Aef | Hotel
                                    <small class="float-right">Date: {{date('Y-m-d', strtotime($data['reservasi']['created_at']))}}</small>
                                </h4>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- info row -->
                        <div class="row invoice-info">
                            <div class="col-sm-2 invoice-col">
                                <h5><strong>Data Pemesan </strong></h5>
                                <address>
                                    <strong>Nama</strong><br>
                                    NIK<br>
                                    Jenis Kelamin<br>
                                    Telephone<br>

                                </address>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                                -
                                <address>
                                    <strong>{{$data['tamu']['nama']}}</strong><br>
                                    {{$data['tamu']['nik']}}<br>
                                    {{$data['tamu']['jenis_kelamin']}}<br>
                                    {{$data['tamu']['telepon']}}<br>

                                </address>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-2 invoice-col">
                                <h5><strong>Data Kamar</strong></h5>

                                <b>Type Kamar:</b><br>
                                <b>Nama Kamar:</b><br>
                                <b>Harga Kamar:</b>
                                <b>Start Date:</b>
                                <b>End Date Kamar:</b>
                            </div>
                            <div class="col-sm-4 invoice-col">
                                <h5><strong>-</strong></h5>

                                {{$data['kamar']['tipeKamar']['tipe_kamar']}}<br>
                                {{$data['kamar']['nama_kamar']}}<br>
                                {{"Rp. ".number_format($data['kamar']['harga'])}}<br>
                                {{$data['reservasi']['start_date']}}<br>
                                {{$data['reservasi']['end_date']}}<br>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        <!-- Table row -->
                        <div class="row">
                            <div class="col-12 table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Tipe Kamar</th>
                                            <th>Nama Kamar</th>
                                            <th>Harga</th>
                                            <th>Qty Kamar</th>
                                            <th>Lama</th>
                                            <th>Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>{{$data['kamar']['tipeKamar']['tipe_kamar']}}</td>
                                            <td>{{$data['kamar']['nama_kamar']}}</td>
                                            <td>{{"Rp. ".number_format($data['kamar']['harga'])}}</td>
                                            <td>{{$data['reservasi']['qty_kamar']}}</td>
                                            <td>{{$data['reservasi']['lama']}}</td>
                                            <td>
                                                <?php
                      $sub =$data['kamar']['harga'] * $data['reservasi']['qty_kamar'] * $data['reservasi']['lama'];
                      ?>
                                                {{"Rp. ".number_format($sub)}}
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->


                        <!-- /.row -->

                        <!-- this row will not appear when printing -->
                        <div class="row no-print">
                            <div class="col-12">
                                @if($data['reservasi']['canceled_by']==null)
                                @if($data['reservasi']['status']=='RESERVASI')
                                <a class="btn btn-success" href="{{ url('reservasi/checkin',$data['reservasi']['id']) }}">
                                    Check-In
                                </a>
                                <a class="btn btn-danger" href="{{ url('reservasi/cancel',$data['reservasi']['id']) }}">
                                    Tolak
                                </a>
                                @elseif($data['reservasi']['status']=='CHECKIN')
                                <a class="btn btn-primary" href="{{ url('reservasi/checkout',$data['reservasi']['id']) }}">
                                    Check-Out
                                </a>
                                @else
                                Selesai
                                @endif
                                @else
                                <p style="font-size:16px; color:red">Ditolak</p>
                                @endif
                                <a class="btn btn-primary float-right" href="{{ url('reservasi/download',$data['reservasi']['id']) }}">
                                    Download
                                </a>

                            </div>
                        </div>
                    </div>
                    <!-- /.invoice -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!--================ Accomodation Area  =================-->
@endsection
