@extends('layouts.app-tamu')
@section('content')
<!--================ Accomodation Area  =================-->
        
        <section class="accomodation_area section_gap">
            <div class="container">
            @foreach($data as $key => $value)
                <div class="section_title text-center">
                    <h2 class="title_color">{{$value['tipe_kamar']}}</h2>
                </div>
                <div class="row accomodation_two">
                    @foreach($data[$key]['kamar'] as $kamar)
                    <?php 
                    $img = 'royal/image/room1.jpg';
                    ?>
                        @foreach($kamar['galery'] as $foto)
                            <?php 
                                $img = 'images/'.$foto['foto'];
                            ?>
                        @endforeach
                    <div class="col-lg-3 col-sm-6">
                        <div class="accomodation_item text-center">
                            <div class="hotel_img">
                                <img src="{{ asset($img)}}" style="width:100%; height:250px" alt="">
                                <a href="{{ url('tamu/kamar/show',$kamar['id']) }}" class="btn theme_btn button_hover">Detail</a>
                            </div>
                            <a href="#"><h4 class="sec_h4">{{$kamar['nama_kamar']}}</h4></a>
                            <h5>{{"Rp. ".number_format($kamar['harga'])}}<small>/hari</small></h5>
                        </div>
                    </div>
                    @endforeach
                    
                    
                </div>
            </div>
            @endforeach
        </section>
        <!--================ Accomodation Area  =================-->
@endsection
