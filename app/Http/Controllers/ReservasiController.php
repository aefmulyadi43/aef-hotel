<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Kamar;
use App\Models\TipeKamar;
use App\Models\KamarGaleri;
use App\Models\Tamu;
use App\Models\User;
use App\Models\Reservasi;
use PDF;
class ReservasiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $reservasi = Reservasi::query()->with(['kamar','tamu'])->whereHas('kamar')->orderBy('id','DESC')->get();
        foreach($reservasi as $row){

        }
        return view('reservasi.index',['data'=>$reservasi]);
    }

    public function checkin($id)
    {
        // dd('disini');

        $reservasi = Reservasi::query()->where('id',$id)->first();
        $reservasi->approved_by = Auth::user()->id;
        $reservasi->approved_date = now();
        $reservasi->status = 'CHECKIN';
        $reservasi->save();

        $kamar = Kamar::query()->where('id',$reservasi->kamar_id)->first();
        $kamar->checkIn($reservasi->qty_kamar);
        return redirect()->route('reservasi.index');
    }

    public function show($id)
    {
        $reservasi = Reservasi::find($id);
        $tamu = Tamu::query()->where('id', $reservasi->tamu_id)->first();
        $kamar = Kamar::query()->where('id',$reservasi->kamar_id)->with(['tipeKamar'])->first();

        $data = [
            'tamu' => $tamu,
            'kamar' => $kamar,
            'reservasi' => $reservasi,
        ];
       return view('reservasi.invoice',['data'=>$data]);
    }

    public function checkout($id)
    {
        // dd('disini');
        $reservasi = Reservasi::query()->where('id',$id)->first();

        $reservasi->status = 'CHECKOUT';
        $reservasi->save();
        $kamar = Kamar::query()->where('id',$reservasi->kamar_id)->first();
        $kamar->checkOut($reservasi->qty_kamar);
        return redirect()->route('reservasi.index');
    }

    public function cancel($id)
    {
        // dd('disini');
        $reservasi = Reservasi::query()->where('id',$id)->first();
        $reservasi->canceled_by = Auth::user()->id;
        $reservasi->canceled_date = now();
        $reservasi->save();

        return redirect()->route('reservasi.index');
    }

    public function add()
    {
        $tipe = TipeKamar::query()->where('status','1')->get();
        return view('kamar.add',['data'=>$tipe,'edit'=>0]);
    }




    public function edit($id)
    {
        $user = User::find($id);
        return view('user.edit',['data'=>$user]);
    }

    public function download($id)
    {
        $reservasi = Reservasi::find($id);
        $tamu = Tamu::query()->where('id', $reservasi->tamu_id)->first();
        $kamar = Kamar::query()->where('id',$reservasi->kamar_id)->with(['tipeKamar'])->first();

        $data = [
            'tamu' => $tamu,
            'kamar' => $kamar,
            'reservasi' => $reservasi,
        ];
    //    return view('reservasi.invoice',['data'=>$data]);

       $pdf = PDF::loadview('reservasi.cetak',['data'=>$data]);
       return $pdf->stream();
    }



    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'tipe' => 'required|string|min:3',
            'status' => 'required',
        ], [
            'tipe.required' => 'Tipe Kamar Tidak Boleh Kosong!',
            'tipe.string' => 'Tipe Kamar Harus Berupa Teks!',
            'status.required' => 'Status Kamar Tidak Boleh Kosong',
            'tipe.min'=>'Tipe Kamar Minimal 3 Karakter'
        ]);

        $tipe = new TipeKamar;
        $tipe->tipe_kamar = $request->tipe;
        $tipe->status = $request->status;
        $tipe->save();
        return redirect()->route('tipe.index')->with('success', 'Data Berhasil Di Tambah');
    }

    public function update(Request $request)
    {

        $user = User::find($request->id);
        $user->role = $request->role;
        $user->update();
        return redirect()->route('user.index')->with('success', 'Data Berhasil Di Tambah');
    }
}
