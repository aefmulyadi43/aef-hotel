<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Kamar;
use App\Models\TipeKamar;
use App\Models\KamarGaleri;
use App\Models\Tamu;
use App\Models\User;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = User::query()->get();
        
        return view('user.index',['data'=>$user]);
    }

    public function add()
    {
        $tipe = TipeKamar::query()->where('status','1')->get();
        return view('kamar.add',['data'=>$tipe,'edit'=>0]);
    }

   

    
    public function edit($id)
    {
        $user = User::find($id);
        return view('user.edit',['data'=>$user]);
    }

    
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'tipe' => 'required|string|min:3',
            'status' => 'required',
        ], [
            'tipe.required' => 'Tipe Kamar Tidak Boleh Kosong!',
            'tipe.string' => 'Tipe Kamar Harus Berupa Teks!',
            'status.required' => 'Status Kamar Tidak Boleh Kosong',
            'tipe.min'=>'Tipe Kamar Minimal 3 Karakter'
        ]);
        
        $tipe = new TipeKamar;
        $tipe->tipe_kamar = $request->tipe;
        $tipe->status = $request->status;
        $tipe->save();
        return redirect()->route('tipe.index')->with('success', 'Data Berhasil Di Tambah');
    }

    public function update(Request $request)
    {
               
        $user = User::find($request->id);
        $user->role = $request->role;
        $user->update();
        return redirect()->route('user.index')->with('success', 'Data Berhasil Di Tambah');
    }
}
