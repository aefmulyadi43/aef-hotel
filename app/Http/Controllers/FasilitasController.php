<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Kamar;
use App\Models\TipeKamar;
use App\Models\KamarGaleri;
use App\Models\Fasilitas;

class FasilitasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $fasilitas = Fasilitas::query()->where('status',1)->get();

        return view('fasilitas.index',['data'=>$fasilitas]);
    }

    public function add()
    {
        return view('fasilitas.add');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama_fasilitas' => 'required',
            'jenis_fasilitas' => 'required',
            'icon' => 'required',
            'gambar' => 'required',
        ], [
            'nama_fasilitas.required' => 'Nama Fasilitas Tidak Boleh Kosong!',
            'jenis_fasilitas.required' => 'Jenis Failitas Tidak Boleh Kosong!',
            'icon.required' => 'Icon Tidak Boleh Kosong!',
            'gambar.required'=>'Gambar Tidak Boleh Kosong!',
        ]);

        $imageName ='gambar-'.time().'.'.$request->file('gambar')->extension();
        $request->file('gambar')->move(public_path('images'), $imageName);

        $iconName = 'icon-'.time().'.'.$request->file('icon')->extension();
        $request->file('icon')->move(public_path('images'), $iconName);

        $fasilitas = new Fasilitas;
        $fasilitas->nama_fasilitas = $request->nama_fasilitas;
        $fasilitas->jenis_fasilitas = $request->jenis_fasilitas;
        $fasilitas->icon = $iconName;
        $fasilitas->gambar = $imageName;
        $fasilitas->save();



        return redirect()->route('fasilitas.index')->with('success', 'Data Berhasil Di Tambah');
    }

    public function update(Request $request)
    {
        $validatedData = $request->validate([
            'nama_fasilitas' => 'required',
            'jenis_fasilitas' => 'required'
        ], [
            'nama_fasilitas.required' => 'Nama Fasilitas Tidak Boleh Kosong!',
            'jenis_fasilitas.required' => 'Jenis Failitas Tidak Boleh Kosong!',
        ]);



        $fasilitas =Fasilitas::find($request->id);
        $fasilitas->nama_fasilitas = $request->nama_fasilitas;
        $fasilitas->jenis_fasilitas = $request->jenis_fasilitas;

        if($request->file('gambar') != null || $request->file('gambar') != ""){
            $imageName ='gambar-'.time().'.'.$request->file('gambar')->extension();
            $request->file('gambar')->move(public_path('images'), $imageName);
            $fasilitas->gambar = $imageName;
        }
        if($request->file('icon') != null || $request->file('icon') != ""){
            $iconName = 'icon-'.time().'.'.$request->file('icon')->extension();
            $request->file('icon')->move(public_path('images'), $iconName);
            $fasilitas->icon = $iconName;
        }

        $fasilitas->update();

        return redirect()->route('fasilitas.index')->with('success', 'Data Berhasil Di Edit');
    }

    public function delete(Request $request)
    {

        $fasilitas =Fasilitas::find($request->id);
        $fasilitas->status = 0;
        $fasilitas->update();
        return redirect()->route('fasilitas.index')->with('success', 'Data Berhasil Di Edit');
    }

    public function show($id)
    {
        $tipe = TipeKamar::query()->where('status','1')->get();
        $old = Fasilitas::find($id);
        return view('fasilitas.edit',['edit'=>1,'old'=>$old]);
    }

}
