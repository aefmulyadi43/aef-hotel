<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tamu extends Model
{
    use HasFactory;

    protected $table = 'tamus';
    protected $fillable = [
        'nik',
        'nama',
        'jenis_kelamin',
        'alamat',
        'telepon',
        'user_id'
    ];

    
}
